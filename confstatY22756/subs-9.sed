:t
  /@[a-zA-Z_][a-zA-Z_0-9]*@/!b
s,@OMPI_BUILD_rmgr_proxy_DSO_TRUE@,,;t t
s,@OMPI_BUILD_rmgr_proxy_DSO_FALSE@,#,;t t
s,@OMPI_BUILD_rmgr_urm_DSO_TRUE@,,;t t
s,@OMPI_BUILD_rmgr_urm_DSO_FALSE@,#,;t t
s,@MCA_rmgr_ALL_SUBDIRS@, proxy urm,;t t
s,@MCA_rmgr_STATIC_SUBDIRS@,,;t t
s,@MCA_rmgr_DSO_SUBDIRS@, proxy urm,;t t
s,@MCA_rmgr_STATIC_LTLIBS@,,;t t
s,@OMPI_BUILD_rml_oob_DSO_TRUE@,,;t t
s,@OMPI_BUILD_rml_oob_DSO_FALSE@,#,;t t
s,@OMPI_BUILD_rml_cnos_DSO_TRUE@,,;t t
s,@OMPI_BUILD_rml_cnos_DSO_FALSE@,#,;t t
s,@MCA_rml_ALL_SUBDIRS@, oob cnos,;t t
s,@MCA_rml_STATIC_SUBDIRS@,,;t t
s,@MCA_rml_DSO_SUBDIRS@, oob,;t t
s,@MCA_rml_STATIC_LTLIBS@,,;t t
s,@MCA_schema_ALL_SUBDIRS@,,;t t
s,@MCA_schema_STATIC_SUBDIRS@,,;t t
s,@MCA_schema_DSO_SUBDIRS@,,;t t
s,@MCA_schema_STATIC_LTLIBS@,,;t t
s,@OMPI_BUILD_sds_env_DSO_TRUE@,,;t t
s,@OMPI_BUILD_sds_env_DSO_FALSE@,#,;t t
s,@OMPI_BUILD_sds_seed_DSO_TRUE@,,;t t
s,@OMPI_BUILD_sds_seed_DSO_FALSE@,#,;t t
s,@OMPI_BUILD_sds_singleton_DSO_TRUE@,,;t t
s,@OMPI_BUILD_sds_singleton_DSO_FALSE@,#,;t t
s,@OMPI_BUILD_sds_slurm_DSO_TRUE@,,;t t
s,@OMPI_BUILD_sds_slurm_DSO_FALSE@,#,;t t
s,@sds_bproc_CPPFLAGS@,,;t t
s,@sds_bproc_LDFLAGS@,,;t t
s,@sds_bproc_LIBS@,,;t t
s,@OMPI_BUILD_sds_bproc_DSO_TRUE@,,;t t
s,@OMPI_BUILD_sds_bproc_DSO_FALSE@,#,;t t
s,@OMPI_BUILD_sds_cnos_DSO_TRUE@,,;t t
s,@OMPI_BUILD_sds_cnos_DSO_FALSE@,#,;t t
s,@OMPI_BUILD_sds_pipe_DSO_TRUE@,,;t t
s,@OMPI_BUILD_sds_pipe_DSO_FALSE@,#,;t t
s,@sds_portals_utcp_CPPFLAGS@,,;t t
s,@sds_portals_utcp_LDFLAGS@,,;t t
s,@sds_portals_utcp_LIBS@,-lutcpapi -lutcplib -lp3api -lp3lib -lp3rt,;t t
s,@OMPI_BUILD_sds_portals_utcp_DSO_TRUE@,,;t t
s,@OMPI_BUILD_sds_portals_utcp_DSO_FALSE@,#,;t t
s,@MCA_sds_ALL_SUBDIRS@, env seed singleton slurm bproc cnos pipe portals_utcp,;t t
s,@MCA_sds_STATIC_SUBDIRS@,,;t t
s,@MCA_sds_DSO_SUBDIRS@, env seed singleton slurm pipe,;t t
s,@MCA_sds_STATIC_LTLIBS@,,;t t
s,@soh_bproc_CPPFLAGS@,,;t t
s,@soh_bproc_LDFLAGS@,,;t t
