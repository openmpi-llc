:t
  /@[a-zA-Z_][a-zA-Z_0-9]*@/!b
s,@OMPI_MPI_OFFSET_KIND@,0,;t t
s,@OMPI_MPI_ADDRESS_KIND@,0,;t t
s,@OMPI_HAVE_POSIX_THREADS_TRUE@,#,;t t
s,@OMPI_HAVE_POSIX_THREADS_FALSE@,,;t t
s,@OMPI_HAVE_SOLARIS_THREADS_TRUE@,#,;t t
s,@OMPI_HAVE_SOLARIS_THREADS_FALSE@,,;t t
s,@LN_S@,ln -s,;t t
s,@LEX@,${SHELL} /home/leslie/openmpi-1.0/config/missing --run flex,;t t
s,@LEXLIB@,,;t t
s,@LEX_OUTPUT_ROOT@,,;t t
s,@CASE_SENSITIVE_FS_TRUE@,,;t t
s,@CASE_SENSITIVE_FS_FALSE@,#,;t t
s,@OMPI_BUILD_maffinity_first_use_DSO_TRUE@,,;t t
s,@OMPI_BUILD_maffinity_first_use_DSO_FALSE@,#,;t t
s,@maffinity_libnuma_CPPFLAGS@,,;t t
s,@maffinity_libnuma_LDFLAGS@,,;t t
s,@maffinity_libnuma_LIBS@,,;t t
s,@OMPI_BUILD_maffinity_libnuma_DSO_TRUE@,,;t t
s,@OMPI_BUILD_maffinity_libnuma_DSO_FALSE@,#,;t t
s,@MCA_maffinity_ALL_SUBDIRS@, first_use libnuma,;t t
s,@MCA_maffinity_STATIC_SUBDIRS@,,;t t
s,@MCA_maffinity_DSO_SUBDIRS@, first_use,;t t
s,@MCA_maffinity_STATIC_LTLIBS@,,;t t
s,@OMPI_BUILD_memory_darwin_DSO_TRUE@,#,;t t
s,@OMPI_BUILD_memory_darwin_DSO_FALSE@,,;t t
s,@memory_malloc_hooks_LIBS@,-ldl,;t t
s,@OMPI_BUILD_memory_malloc_hooks_DSO_TRUE@,#,;t t
s,@OMPI_BUILD_memory_malloc_hooks_DSO_FALSE@,,;t t
s,@OMPI_BUILD_memory_ptmalloc2_DSO_TRUE@,#,;t t
s,@OMPI_BUILD_memory_ptmalloc2_DSO_FALSE@,,;t t
s,@memory_malloc_interpose_LIBS@,,;t t
s,@OMPI_BUILD_memory_malloc_interpose_DSO_TRUE@,#,;t t
s,@OMPI_BUILD_memory_malloc_interpose_DSO_FALSE@,,;t t
s,@MCA_memory_ALL_SUBDIRS@, darwin malloc_hooks ptmalloc2 malloc_interpose,;t t
s,@MCA_memory_STATIC_SUBDIRS@, malloc_hooks,;t t
s,@MCA_memory_DSO_SUBDIRS@,,;t t
s,@MCA_memory_STATIC_LTLIBS@,mca/memory/malloc_hooks/libmca_memory_malloc_hooks.la ,;t t
s,@OMPI_BUILD_paffinity_linux_DSO_TRUE@,,;t t
s,@OMPI_BUILD_paffinity_linux_DSO_FALSE@,#,;t t
s,@OMPI_BUILD_paffinity_solaris_DSO_TRUE@,,;t t
s,@OMPI_BUILD_paffinity_solaris_DSO_FALSE@,#,;t t
s,@MCA_paffinity_ALL_SUBDIRS@, linux solaris,;t t
s,@MCA_paffinity_STATIC_SUBDIRS@,,;t t
s,@MCA_paffinity_DSO_SUBDIRS@, linux,;t t
s,@MCA_paffinity_STATIC_LTLIBS@,,;t t
s,@timer_aix_LIBS@,,;t t
s,@OMPI_BUILD_timer_aix_DSO_TRUE@,#,;t t
s,@OMPI_BUILD_timer_aix_DSO_FALSE@,,;t t
