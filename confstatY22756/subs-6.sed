:t
  /@[a-zA-Z_][a-zA-Z_0-9]*@/!b
s,@OMPI_BUILD_timer_altix_DSO_TRUE@,#,;t t
s,@OMPI_BUILD_timer_altix_DSO_FALSE@,,;t t
s,@OMPI_BUILD_timer_darwin_DSO_TRUE@,#,;t t
s,@OMPI_BUILD_timer_darwin_DSO_FALSE@,,;t t
s,@OMPI_BUILD_timer_solaris_DSO_TRUE@,#,;t t
s,@OMPI_BUILD_timer_solaris_DSO_FALSE@,,;t t
s,@OMPI_BUILD_timer_linux_DSO_TRUE@,#,;t t
s,@OMPI_BUILD_timer_linux_DSO_FALSE@,,;t t
s,@MCA_timer_ALL_SUBDIRS@, aix altix darwin solaris linux,;t t
s,@MCA_timer_STATIC_SUBDIRS@, linux,;t t
s,@MCA_timer_DSO_SUBDIRS@,,;t t
s,@MCA_timer_STATIC_LTLIBS@,mca/timer/linux/libmca_timer_linux.la ,;t t
s,@MCA_opal_FRAMEWORKS@, maffinity memory paffinity timer,;t t
s,@MCA_opal_FRAMEWORK_LIBS@, mca/maffinity/base/libmca_maffinity_base.la $(MCA_maffinity_STATIC_LTLIBS) mca/memory/base/libmca_memory_base.la $(MCA_memory_STATIC_LTLIBS) mca/paffinity/base/libmca_paffinity_base.la $(MCA_paffinity_STATIC_LTLIBS) mca/timer/base/libmca_timer_base.la $(MCA_timer_STATIC_LTLIBS),;t t
s,@MCA_errmgr_ALL_SUBDIRS@,,;t t
s,@MCA_errmgr_STATIC_SUBDIRS@,,;t t
s,@MCA_errmgr_DSO_SUBDIRS@,,;t t
s,@MCA_errmgr_STATIC_LTLIBS@,,;t t
s,@OMPI_BUILD_gpr_null_DSO_TRUE@,,;t t
s,@OMPI_BUILD_gpr_null_DSO_FALSE@,#,;t t
s,@OMPI_BUILD_gpr_proxy_DSO_TRUE@,,;t t
s,@OMPI_BUILD_gpr_proxy_DSO_FALSE@,#,;t t
s,@OMPI_BUILD_gpr_replica_DSO_TRUE@,,;t t
s,@OMPI_BUILD_gpr_replica_DSO_FALSE@,#,;t t
s,@MCA_gpr_ALL_SUBDIRS@, null proxy replica,;t t
s,@MCA_gpr_STATIC_SUBDIRS@,,;t t
s,@MCA_gpr_DSO_SUBDIRS@, null proxy replica,;t t
s,@MCA_gpr_STATIC_LTLIBS@,,;t t
s,@OMPI_BUILD_iof_proxy_DSO_TRUE@,,;t t
s,@OMPI_BUILD_iof_proxy_DSO_FALSE@,#,;t t
s,@OMPI_BUILD_iof_svc_DSO_TRUE@,,;t t
s,@OMPI_BUILD_iof_svc_DSO_FALSE@,#,;t t
s,@MCA_iof_ALL_SUBDIRS@, proxy svc,;t t
s,@MCA_iof_STATIC_SUBDIRS@,,;t t
s,@MCA_iof_DSO_SUBDIRS@, proxy svc,;t t
s,@MCA_iof_STATIC_LTLIBS@,,;t t
s,@OMPI_BUILD_ns_proxy_DSO_TRUE@,,;t t
s,@OMPI_BUILD_ns_proxy_DSO_FALSE@,#,;t t
s,@OMPI_BUILD_ns_replica_DSO_TRUE@,,;t t
s,@OMPI_BUILD_ns_replica_DSO_FALSE@,#,;t t
s,@MCA_ns_ALL_SUBDIRS@, proxy replica,;t t
s,@MCA_ns_STATIC_SUBDIRS@,,;t t
s,@MCA_ns_DSO_SUBDIRS@, proxy replica,;t t
s,@MCA_ns_STATIC_LTLIBS@,,;t t
s,@OMPI_BUILD_oob_tcp_DSO_TRUE@,,;t t
s,@OMPI_BUILD_oob_tcp_DSO_FALSE@,#,;t t
s,@OMPI_BUILD_oob_llc_DSO_TRUE@,,;t t
s,@OMPI_BUILD_oob_llc_DSO_FALSE@,#,;t t
