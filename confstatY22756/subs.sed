s,@SHELL@,/bin/sh,;t t
s,@PATH_SEPARATOR@,:,;t t
s,@PACKAGE_NAME@,Open MPI,;t t
s,@PACKAGE_TARNAME@,openmpi,;t t
s,@PACKAGE_VERSION@,1.0,;t t
s,@PACKAGE_STRING@,Open MPI 1.0,;t t
s,@PACKAGE_BUGREPORT@,http://www.open-mpi.org/community/help/,;t t
s,@exec_prefix@,${prefix},;t t
s,@prefix@,/usr/local,;t t
s,@program_transform_name@,s\,x\,x\,,;t t
s,@bindir@,${exec_prefix}/bin,;t t
s,@sbindir@,${exec_prefix}/sbin,;t t
s,@libexecdir@,${exec_prefix}/libexec,;t t
s,@datadir@,${prefix}/share,;t t
s,@sysconfdir@,${prefix}/etc,;t t
s,@sharedstatedir@,${prefix}/com,;t t
s,@localstatedir@,${prefix}/var,;t t
s,@libdir@,${exec_prefix}/lib,;t t
s,@includedir@,${prefix}/include,;t t
s,@oldincludedir@,/usr/include,;t t
s,@infodir@,${prefix}/info,;t t
s,@mandir@,${prefix}/man,;t t
s,@build_alias@,,;t t
s,@host_alias@,,;t t
s,@target_alias@,,;t t
s,@DEFS@,-DHAVE_CONFIG_H,;t t
s,@ECHO_C@,,;t t
s,@ECHO_N@,-n,;t t
s,@ECHO_T@,,;t t
s,@LIBS@,-lm  -lutil -lnsl ,;t t
s,@INSTALL_PROGRAM@,${INSTALL},;t t
s,@INSTALL_SCRIPT@,${INSTALL},;t t
s,@INSTALL_DATA@,${INSTALL} -m 644,;t t
s,@CYGPATH_W@,echo,;t t
s,@PACKAGE@,openmpi,;t t
s,@VERSION@,1.0,;t t
s,@ACLOCAL@,${SHELL} /home/leslie/openmpi-1.0/config/missing --run aclocal-1.9,;t t
s,@AUTOCONF@,${SHELL} /home/leslie/openmpi-1.0/config/missing --run autoconf,;t t
s,@AUTOMAKE@,${SHELL} /home/leslie/openmpi-1.0/config/missing --run automake-1.9,;t t
s,@AUTOHEADER@,${SHELL} /home/leslie/openmpi-1.0/config/missing --run autoheader,;t t
s,@MAKEINFO@,${SHELL} /home/leslie/openmpi-1.0/config/missing --run makeinfo,;t t
s,@install_sh@,/home/leslie/openmpi-1.0/config/install-sh,;t t
s,@STRIP@,strip,;t t
s,@ac_ct_STRIP@,strip,;t t
s,@INSTALL_STRIP_PROGRAM@,${SHELL} $(install_sh) -c -s,;t t
s,@mkdir_p@,mkdir -p --,;t t
s,@AWK@,gawk,;t t
s,@SET_MAKE@,,;t t
s,@am__leading_dot@,.,;t t
s,@AMTAR@,${SHELL} /home/leslie/openmpi-1.0/config/missing --run tar,;t t
s,@am__tar@,${AMTAR} chof - "$$tardir",;t t
s,@am__untar@,${AMTAR} xf -,;t t
s,@CONFIGURE_DEPENDENCIES@,$(top_srcdir)/VERSION,;t t
s,@OMPI_MAJOR_VERSION@,1,;t t
s,@OMPI_MINOR_VERSION@,0,;t t
s,@OMPI_RELEASE_VERSION@,0,;t t
s,@OMPI_GREEK_VERSION@,,;t t
s,@OMPI_WANT_SVN@,0,;t t
s,@OMPI_SVN_R@,r8189,;t t
s,@OMPI_VERSION@,1.0,;t t
s,@ORTE_MAJOR_VERSION@,1,;t t
s,@ORTE_MINOR_VERSION@,0,;t t
s,@ORTE_RELEASE_VERSION@,0,;t t
s,@ORTE_GREEK_VERSION@,,;t t
s,@ORTE_WANT_SVN@,0,;t t
s,@ORTE_SVN_R@,r8189,;t t
s,@ORTE_VERSION@,1.0,;t t
s,@OPAL_MAJOR_VERSION@,1,;t t
s,@OPAL_MINOR_VERSION@,0,;t t
s,@OPAL_RELEASE_VERSION@,0,;t t
s,@OPAL_GREEK_VERSION@,,;t t
s,@OPAL_WANT_SVN@,0,;t t
s,@OPAL_SVN_R@,r8189,;t t
s,@OPAL_VERSION@,1.0,;t t
s,@OMPI_CONFIGURE_USER@,leslie,;t t
s,@OMPI_CONFIGURE_HOST@,mercurio,;t t
s,@OMPI_CONFIGURE_DATE@,Thu Dec 15 16:55:25 BRST 2005,;t t
s,@OMPI_TOP_BUILDDIR@,/home/leslie/openmpi-1.0,;t t
s,@OMPI_TOP_SRCDIR@,/home/leslie/openmpi-1.0,;t t
s,@CLEANFILES@,*~ .\\#*,;t t
s,@build@,i686-pc-linux-gnu,;t t
s,@build_cpu@,i686,;t t
s,@build_vendor@,pc,;t t
s,@build_os@,linux-gnu,;t t
s,@host@,i686-pc-linux-gnu,;t t
s,@host_cpu@,i686,;t t
s,@host_vendor@,pc,;t t
s,@host_os@,linux-gnu,;t t
s,@ac_prefix_program@,,;t t
s,@top_ompi_srcdir@,/home/leslie/openmpi-1.0,;t t
s,@top_ompi_builddir@,/home/leslie/openmpi-1.0,;t t
s,@MPIF_H_PMPI_W_FUNCS@,\, PMPI_WTICK\, PMPI_WTIME,;t t
s,@WANT_INSTALL_HEADERS_TRUE@,#,;t t
s,@WANT_INSTALL_HEADERS_FALSE@,,;t t
s,@WANT_DEPRECATED_EXECUTABLE_NAMES_TRUE@,#,;t t
s,@WANT_DEPRECATED_EXECUTABLE_NAMES_FALSE@,,;t t
s,@WANT_MPI2_ONE_SIDED_TRUE@,#,;t t
s,@WANT_MPI2_ONE_SIDED_FALSE@,,;t t
s,@OMPI_F77_WIN_ATTR_KEYS@,,;t t
s,@OMPI_F77_WIN_ATTR_BASE_VALUE@,,;t t
s,@OMPI_F77_WIN_ATTR_SIZE_VALUE@,,;t t
s,@OMPI_F77_WIN_ATTR_DISP_VALUE@,,;t t
s,@OMPI_F77_WIN_NULL_COPY_FN@,,;t t
s,@OMPI_F77_WIN_NULL_DELETE_FN@,,;t t
s,@OMPI_F77_WIN_DUP_FN@,,;t t
s,@OMPI_FORTRAN_MAX_ARRAY_RANK@,4,;t t
s,@CC@,gcc,;t t
s,@CFLAGS@,-O3 -DNDEBUG -fno-strict-aliasing -pthread,;t t
s,@LDFLAGS@,-export-dynamic  ,;t t
s,@CPPFLAGS@,-I$(top_srcdir)/include -I$(top_srcdir) -I$(top_builddir) -I$(top_builddir)/include -I$(top_srcdir)/opal -I$(top_srcdir)/orte -I$(top_srcdir)/ompi  ,;t t
s,@ac_ct_CC@,gcc,;t t
s,@EXEEXT@,,;t t
s,@OBJEXT@,o,;t t
s,@DEPDIR@,.deps,;t t
s,@am__include@,include,;t t
s,@am__quote@,,;t t
s,@AMDEP_TRUE@,,;t t
s,@AMDEP_FALSE@,#,;t t
s,@AMDEPBACKSLASH@,\\,;t t
s,@CCDEPMODE@,depmode=gcc3,;t t
s,@am__fastdepCC_TRUE@,,;t t
s,@am__fastdepCC_FALSE@,#,;t t
s,@OMPI_CC_ABSOLUTE@,/usr/bin/gcc,;t t
s,@CPP@,gcc -E,;t t
s,@EGREP@,grep -E,;t t
s,@WANT_MPI_BINDINGS_LAYER_TRUE@,#,;t t
s,@WANT_MPI_BINDINGS_LAYER_FALSE@,,;t t
s,@WANT_PMPI_BINDINGS_LAYER_TRUE@,,;t t
s,@WANT_PMPI_BINDINGS_LAYER_FALSE@,#,;t t
s,@COMPILE_PROFILING_SEPARATELY_TRUE@,#,;t t
s,@COMPILE_PROFILING_SEPARATELY_FALSE@,,;t t
s,@CXX@,g++,;t t
s,@CXXFLAGS@,-O3 -DNDEBUG -finline-functions -pthread,;t t
s,@ac_ct_CXX@,g++,;t t
s,@CXXDEPMODE@,depmode=gcc3,;t t
s,@am__fastdepCXX_TRUE@,,;t t
s,@am__fastdepCXX_FALSE@,#,;t t
s,@CXXCPP@,g++ -E,;t t
s,@OMPI_CXX_ABSOLUTE@,/usr/bin/g++,;t t
s,@WANT_MPI_CXX_BINDINGS_TRUE@,,;t t
s,@WANT_MPI_CXX_BINDINGS_FALSE@,#,;t t
s,@CCAS@,gcc,;t t
s,@CCASFLAGS@,-O3 -DNDEBUG -fno-strict-aliasing,;t t
s,@OMPI_ASM_TEXT@,.text,;t t
s,@OMPI_AS_GLOBAL@,,;t t
s,@OMPI_AS_LABEL_SUFFIX@,,;t t
s,@OMPI_ASM_GSYM@,,;t t
s,@OMPI_ASM_LSYM@,.L,;t t
s,@OMPI_ASM_TYPE@,@,;t t
s,@OMPI_ASM_SUPPORT_64BIT@,1,;t t
s,@OMPI_ASSEMBLY_FORMAT@,default-.text-.globl-:--.L-@-1-0-1-1,;t t
s,@OMPI_ASSEMBLY_ARCH@,IA32,;t t
s,@FGREP@,grep -F,;t t
s,@PERL@,perl,;t t
s,@OMPI_HAVE_ASM_FILE_TRUE@,,;t t
s,@OMPI_HAVE_ASM_FILE_FALSE@,#,;t t
s,@OMPI_ASM_FILE@,atomic-ia32-linux.s,;t t
s,@F77@,,;t t
s,@FFLAGS@,,;t t
s,@ac_ct_F77@,,;t t
s,@OMPI_WANT_F77_BINDINGS_TRUE@,#,;t t
s,@OMPI_WANT_F77_BINDINGS_FALSE@,,;t t
s,@OMPI_F77_ABSOLUTE@,none,;t t
s,@WANT_MPI_F77_BINDINGS_LAYER_TRUE@,#,;t t
s,@WANT_MPI_F77_BINDINGS_LAYER_FALSE@,,;t t
s,@WANT_PMPI_F77_BINDINGS_LAYER_TRUE@,#,;t t
s,@WANT_PMPI_F77_BINDINGS_LAYER_FALSE@,,;t t
s,@FC@,,;t t
s,@FCFLAGS@,,;t t
s,@ac_ct_FC@,,;t t
s,@FCFLAGS_f@,,;t t
s,@FCFLAGS_f90@,,;t t
s,@FCFLAGS_f95@,,;t t
s,@OMPI_WANT_F90_BINDINGS_TRUE@,#,;t t
s,@OMPI_WANT_F90_BINDINGS_FALSE@,,;t t
s,@OMPI_F90_ABSOLUTE@,none,;t t
s,@OMPI_FC_MODULE_FLAG@,,;t t
s,@OMPI_FORTRAN_LKINDS@,,;t t
s,@OMPI_FORTRAN_IKINDS@,,;t t
s,@OMPI_FORTRAN_RKINDS@,,;t t
s,@OMPI_FORTRAN_CKINDS@,,;t t
s,@OMPI_SIZEOF_F90_INT1@,0,;t t
s,@OMPI_SIZEOF_F90_INT2@,0,;t t
s,@OMPI_SIZEOF_F90_INT4@,0,;t t
s,@OMPI_SIZEOF_F90_INT8@,0,;t t
s,@OMPI_SIZEOF_F90_INT16@,0,;t t
s,@OMPI_SIZEOF_F90_REAL4@,0,;t t
s,@OMPI_SIZEOF_F90_REAL8@,0,;t t
s,@OMPI_SIZEOF_F90_REAL16@,0,;t t
s,@OMPI_SIZEOF_F90_COMPLEX8@,0,;t t
s,@OMPI_SIZEOF_F90_COMPLEX16@,0,;t t
s,@OMPI_SIZEOF_F90_COMPLEX32@,0,;t t
s,@OMPI_MPI_OFFSET_KIND@,0,;t t
s,@OMPI_MPI_ADDRESS_KIND@,0,;t t
s,@OMPI_HAVE_POSIX_THREADS_TRUE@,#,;t t
s,@OMPI_HAVE_POSIX_THREADS_FALSE@,,;t t
s,@OMPI_HAVE_SOLARIS_THREADS_TRUE@,#,;t t
s,@OMPI_HAVE_SOLARIS_THREADS_FALSE@,,;t t
s,@LN_S@,ln -s,;t t
s,@LEX@,${SHELL} /home/leslie/openmpi-1.0/config/missing --run flex,;t t
s,@LEXLIB@,,;t t
s,@LEX_OUTPUT_ROOT@,,;t t
s,@CASE_SENSITIVE_FS_TRUE@,,;t t
s,@CASE_SENSITIVE_FS_FALSE@,#,;t t
s,@OMPI_BUILD_maffinity_first_use_DSO_TRUE@,,;t t
s,@OMPI_BUILD_maffinity_first_use_DSO_FALSE@,#,;t t
s,@maffinity_libnuma_CPPFLAGS@,,;t t
s,@maffinity_libnuma_LDFLAGS@,,;t t
s,@maffinity_libnuma_LIBS@,,;t t
s,@OMPI_BUILD_maffinity_libnuma_DSO_TRUE@,,;t t
s,@OMPI_BUILD_maffinity_libnuma_DSO_FALSE@,#,;t t
s,@MCA_maffinity_ALL_SUBDIRS@, first_use libnuma,;t t
s,@MCA_maffinity_STATIC_SUBDIRS@,,;t t
s,@MCA_maffinity_DSO_SUBDIRS@, first_use,;t t
s,@MCA_maffinity_STATIC_LTLIBS@,,;t t
s,@OMPI_BUILD_memory_darwin_DSO_TRUE@,#,;t t
s,@OMPI_BUILD_memory_darwin_DSO_FALSE@,,;t t
s,@memory_malloc_hooks_LIBS@,-ldl,;t t
s,@OMPI_BUILD_memory_malloc_hooks_DSO_TRUE@,#,;t t
s,@OMPI_BUILD_memory_malloc_hooks_DSO_FALSE@,,;t t
s,@OMPI_BUILD_memory_ptmalloc2_DSO_TRUE@,#,;t t
s,@OMPI_BUILD_memory_ptmalloc2_DSO_FALSE@,,;t t
s,@memory_malloc_interpose_LIBS@,,;t t
s,@OMPI_BUILD_memory_malloc_interpose_DSO_TRUE@,#,;t t
s,@OMPI_BUILD_memory_malloc_interpose_DSO_FALSE@,,;t t
s,@MCA_memory_ALL_SUBDIRS@, darwin malloc_hooks ptmalloc2 malloc_interpose,;t t
s,@MCA_memory_STATIC_SUBDIRS@, malloc_hooks,;t t
s,@MCA_memory_DSO_SUBDIRS@,,;t t
s,@MCA_memory_STATIC_LTLIBS@,mca/memory/malloc_hooks/libmca_memory_malloc_hooks.la ,;t t
s,@OMPI_BUILD_paffinity_linux_DSO_TRUE@,,;t t
s,@OMPI_BUILD_paffinity_linux_DSO_FALSE@,#,;t t
s,@OMPI_BUILD_paffinity_solaris_DSO_TRUE@,,;t t
s,@OMPI_BUILD_paffinity_solaris_DSO_FALSE@,#,;t t
s,@MCA_paffinity_ALL_SUBDIRS@, linux solaris,;t t
s,@MCA_paffinity_STATIC_SUBDIRS@,,;t t
s,@MCA_paffinity_DSO_SUBDIRS@, linux,;t t
s,@MCA_paffinity_STATIC_LTLIBS@,,;t t
s,@timer_aix_LIBS@,,;t t
s,@OMPI_BUILD_timer_aix_DSO_TRUE@,#,;t t
s,@OMPI_BUILD_timer_aix_DSO_FALSE@,,;t t
s,@OMPI_BUILD_timer_altix_DSO_TRUE@,#,;t t
s,@OMPI_BUILD_timer_altix_DSO_FALSE@,,;t t
s,@OMPI_BUILD_timer_darwin_DSO_TRUE@,#,;t t
s,@OMPI_BUILD_timer_darwin_DSO_FALSE@,,;t t
s,@OMPI_BUILD_timer_solaris_DSO_TRUE@,#,;t t
s,@OMPI_BUILD_timer_solaris_DSO_FALSE@,,;t t
s,@OMPI_BUILD_timer_linux_DSO_TRUE@,#,;t t
s,@OMPI_BUILD_timer_linux_DSO_FALSE@,,;t t
s,@MCA_timer_ALL_SUBDIRS@, aix altix darwin solaris linux,;t t
s,@MCA_timer_STATIC_SUBDIRS@, linux,;t t
s,@MCA_timer_DSO_SUBDIRS@,,;t t
s,@MCA_timer_STATIC_LTLIBS@,mca/timer/linux/libmca_timer_linux.la ,;t t
s,@MCA_opal_FRAMEWORKS@, maffinity memory paffinity timer,;t t
s,@MCA_opal_FRAMEWORK_LIBS@, mca/maffinity/base/libmca_maffinity_base.la $(MCA_maffinity_STATIC_LTLIBS) mca/memory/base/libmca_memory_base.la $(MCA_memory_STATIC_LTLIBS) mca/paffinity/base/libmca_paffinity_base.la $(MCA_paffinity_STATIC_LTLIBS) mca/timer/base/libmca_timer_base.la $(MCA_timer_STATIC_LTLIBS),;t t
s,@MCA_errmgr_ALL_SUBDIRS@,,;t t
s,@MCA_errmgr_STATIC_SUBDIRS@,,;t t
s,@MCA_errmgr_DSO_SUBDIRS@,,;t t
s,@MCA_errmgr_STATIC_LTLIBS@,,;t t
s,@OMPI_BUILD_gpr_null_DSO_TRUE@,,;t t
s,@OMPI_BUILD_gpr_null_DSO_FALSE@,#,;t t
s,@OMPI_BUILD_gpr_proxy_DSO_TRUE@,,;t t
s,@OMPI_BUILD_gpr_proxy_DSO_FALSE@,#,;t t
s,@OMPI_BUILD_gpr_replica_DSO_TRUE@,,;t t
s,@OMPI_BUILD_gpr_replica_DSO_FALSE@,#,;t t
s,@MCA_gpr_ALL_SUBDIRS@, null proxy replica,;t t
s,@MCA_gpr_STATIC_SUBDIRS@,,;t t
s,@MCA_gpr_DSO_SUBDIRS@, null proxy replica,;t t
s,@MCA_gpr_STATIC_LTLIBS@,,;t t
s,@OMPI_BUILD_iof_proxy_DSO_TRUE@,,;t t
s,@OMPI_BUILD_iof_proxy_DSO_FALSE@,#,;t t
s,@OMPI_BUILD_iof_svc_DSO_TRUE@,,;t t
s,@OMPI_BUILD_iof_svc_DSO_FALSE@,#,;t t
s,@MCA_iof_ALL_SUBDIRS@, proxy svc,;t t
s,@MCA_iof_STATIC_SUBDIRS@,,;t t
s,@MCA_iof_DSO_SUBDIRS@, proxy svc,;t t
s,@MCA_iof_STATIC_LTLIBS@,,;t t
s,@OMPI_BUILD_ns_proxy_DSO_TRUE@,,;t t
s,@OMPI_BUILD_ns_proxy_DSO_FALSE@,#,;t t
s,@OMPI_BUILD_ns_replica_DSO_TRUE@,,;t t
s,@OMPI_BUILD_ns_replica_DSO_FALSE@,#,;t t
s,@MCA_ns_ALL_SUBDIRS@, proxy replica,;t t
s,@MCA_ns_STATIC_SUBDIRS@,,;t t
s,@MCA_ns_DSO_SUBDIRS@, proxy replica,;t t
s,@MCA_ns_STATIC_LTLIBS@,,;t t
s,@OMPI_BUILD_oob_tcp_DSO_TRUE@,,;t t
s,@OMPI_BUILD_oob_tcp_DSO_FALSE@,#,;t t
s,@OMPI_BUILD_oob_llc_DSO_TRUE@,,;t t
s,@OMPI_BUILD_oob_llc_DSO_FALSE@,#,;t t
s,@MCA_oob_ALL_SUBDIRS@, tcp,;t t
s,@MCA_oob_ALL_SUBDIRS@, llc,;t t
s,@MCA_oob_STATIC_SUBDIRS@,,;t t
s,@MCA_oob_DSO_SUBDIRS@, tcp,;t t
s,@MCA_oob_DSO_SUBDIRS@, llc,;t t
s,@MCA_oob_STATIC_LTLIBS@,,;t t
s,@OMPI_BUILD_pls_daemon_DSO_TRUE@,,;t t
s,@OMPI_BUILD_pls_daemon_DSO_FALSE@,#,;t t
s,@OMPI_BUILD_pls_proxy_DSO_TRUE@,,;t t
s,@OMPI_BUILD_pls_proxy_DSO_FALSE@,#,;t t
s,@pls_bproc_CPPFLAGS@,,;t t
s,@pls_bproc_LDFLAGS@,,;t t
s,@pls_bproc_LIBS@,,;t t
s,@OMPI_BUILD_pls_bproc_DSO_TRUE@,,;t t
s,@OMPI_BUILD_pls_bproc_DSO_FALSE@,#,;t t
s,@pls_bproc_orted_CPPFLAGS@,,;t t
s,@pls_bproc_orted_LDFLAGS@,,;t t
s,@pls_bproc_orted_LIBS@,,;t t
s,@OMPI_BUILD_pls_bproc_orted_DSO_TRUE@,,;t t
s,@OMPI_BUILD_pls_bproc_orted_DSO_FALSE@,#,;t t
s,@OMPI_BUILD_pls_fork_DSO_TRUE@,,;t t
s,@OMPI_BUILD_pls_fork_DSO_FALSE@,#,;t t
s,@OMPI_BUILD_pls_poe_DSO_TRUE@,,;t t
s,@OMPI_BUILD_pls_poe_DSO_FALSE@,#,;t t
s,@OMPI_BUILD_pls_rsh_DSO_TRUE@,,;t t
s,@OMPI_BUILD_pls_rsh_DSO_FALSE@,#,;t t
s,@pls_slurm_CPPFLAGS@,,;t t
s,@pls_slurm_LDFLAGS@,,;t t
s,@pls_slurm_LIBS@,,;t t
s,@OMPI_BUILD_pls_slurm_DSO_TRUE@,,;t t
s,@OMPI_BUILD_pls_slurm_DSO_FALSE@,#,;t t
s,@pls_tm_CPPFLAGS@,,;t t
s,@pls_tm_LDFLAGS@,,;t t
s,@pls_tm_LIBS@,,;t t
s,@OMPI_BUILD_pls_tm_DSO_TRUE@,,;t t
s,@OMPI_BUILD_pls_tm_DSO_FALSE@,#,;t t
s,@OBJC@,gcc,;t t
s,@OBJCFLAGS@,,;t t
s,@ac_ct_OBJC@,gcc,;t t
s,@OBJCDEPMODE@,depmode=gcc3,;t t
s,@am__fastdepOBJC_TRUE@,,;t t
s,@am__fastdepOBJC_FALSE@,#,;t t
s,@pls_xgrid_OBJCFLAGS@,,;t t
s,@pls_xgrid_LDFLAGS@,,;t t
s,@OMPI_BUILD_pls_xgrid_DSO_TRUE@,,;t t
s,@OMPI_BUILD_pls_xgrid_DSO_FALSE@,#,;t t
s,@MCA_pls_ALL_SUBDIRS@, daemon proxy bproc bproc_orted fork poe rsh slurm tm xgrid,;t t
s,@MCA_pls_STATIC_SUBDIRS@,,;t t
s,@MCA_pls_DSO_SUBDIRS@, daemon proxy fork rsh slurm,;t t
s,@MCA_pls_STATIC_LTLIBS@,,;t t
s,@OMPI_BUILD_ras_dash_host_DSO_TRUE@,,;t t
s,@OMPI_BUILD_ras_dash_host_DSO_FALSE@,#,;t t
s,@OMPI_BUILD_ras_hostfile_DSO_TRUE@,,;t t
s,@OMPI_BUILD_ras_hostfile_DSO_FALSE@,#,;t t
s,@OMPI_BUILD_ras_localhost_DSO_TRUE@,,;t t
s,@OMPI_BUILD_ras_localhost_DSO_FALSE@,#,;t t
s,@ras_bjs_CPPFLAGS@,,;t t
s,@ras_bjs_LDFLAGS@,,;t t
s,@ras_bjs_LIBS@,,;t t
s,@OMPI_BUILD_ras_bjs_DSO_TRUE@,,;t t
s,@OMPI_BUILD_ras_bjs_DSO_FALSE@,#,;t t
s,@ras_lsf_bproc_CPPFLAGS@,,;t t
s,@ras_lsf_bproc_LDFLAGS@,,;t t
s,@ras_lsf_bproc_LIBS@,,;t t
s,@OMPI_BUILD_ras_lsf_bproc_DSO_TRUE@,,;t t
s,@OMPI_BUILD_ras_lsf_bproc_DSO_FALSE@,#,;t t
s,@ras_slurm_CPPFLAGS@,,;t t
s,@ras_slurm_LDFLAGS@,,;t t
s,@ras_slurm_LIBS@,,;t t
s,@OMPI_BUILD_ras_slurm_DSO_TRUE@,,;t t
s,@OMPI_BUILD_ras_slurm_DSO_FALSE@,#,;t t
s,@ras_tm_CPPFLAGS@,,;t t
s,@ras_tm_LDFLAGS@,,;t t
s,@ras_tm_LIBS@,,;t t
s,@OMPI_BUILD_ras_tm_DSO_TRUE@,,;t t
s,@OMPI_BUILD_ras_tm_DSO_FALSE@,#,;t t
s,@OMPI_BUILD_ras_xgrid_DSO_TRUE@,,;t t
s,@OMPI_BUILD_ras_xgrid_DSO_FALSE@,#,;t t
s,@MCA_ras_ALL_SUBDIRS@, dash_host hostfile localhost bjs lsf_bproc slurm tm xgrid,;t t
s,@MCA_ras_STATIC_SUBDIRS@,,;t t
s,@MCA_ras_DSO_SUBDIRS@, dash_host hostfile localhost slurm,;t t
s,@MCA_ras_STATIC_LTLIBS@,,;t t
s,@OMPI_BUILD_rds_hostfile_DSO_TRUE@,,;t t
s,@OMPI_BUILD_rds_hostfile_DSO_FALSE@,#,;t t
s,@OMPI_BUILD_rds_resfile_DSO_TRUE@,,;t t
s,@OMPI_BUILD_rds_resfile_DSO_FALSE@,#,;t t
s,@MCA_rds_ALL_SUBDIRS@, hostfile resfile,;t t
s,@MCA_rds_STATIC_SUBDIRS@,,;t t
s,@MCA_rds_DSO_SUBDIRS@, hostfile resfile,;t t
s,@MCA_rds_STATIC_LTLIBS@,,;t t
s,@OMPI_BUILD_rmaps_round_robin_DSO_TRUE@,,;t t
s,@OMPI_BUILD_rmaps_round_robin_DSO_FALSE@,#,;t t
s,@MCA_rmaps_ALL_SUBDIRS@, round_robin,;t t
s,@MCA_rmaps_STATIC_SUBDIRS@,,;t t
s,@MCA_rmaps_DSO_SUBDIRS@, round_robin,;t t
s,@MCA_rmaps_STATIC_LTLIBS@,,;t t
s,@OMPI_BUILD_rmgr_proxy_DSO_TRUE@,,;t t
s,@OMPI_BUILD_rmgr_proxy_DSO_FALSE@,#,;t t
s,@OMPI_BUILD_rmgr_urm_DSO_TRUE@,,;t t
s,@OMPI_BUILD_rmgr_urm_DSO_FALSE@,#,;t t
s,@MCA_rmgr_ALL_SUBDIRS@, proxy urm,;t t
s,@MCA_rmgr_STATIC_SUBDIRS@,,;t t
s,@MCA_rmgr_DSO_SUBDIRS@, proxy urm,;t t
s,@MCA_rmgr_STATIC_LTLIBS@,,;t t
s,@OMPI_BUILD_rml_oob_DSO_TRUE@,,;t t
s,@OMPI_BUILD_rml_oob_DSO_FALSE@,#,;t t
s,@OMPI_BUILD_rml_cnos_DSO_TRUE@,,;t t
s,@OMPI_BUILD_rml_cnos_DSO_FALSE@,#,;t t
s,@MCA_rml_ALL_SUBDIRS@, oob cnos,;t t
s,@MCA_rml_STATIC_SUBDIRS@,,;t t
s,@MCA_rml_DSO_SUBDIRS@, oob,;t t
s,@MCA_rml_STATIC_LTLIBS@,,;t t
s,@MCA_schema_ALL_SUBDIRS@,,;t t
s,@MCA_schema_STATIC_SUBDIRS@,,;t t
s,@MCA_schema_DSO_SUBDIRS@,,;t t
s,@MCA_schema_STATIC_LTLIBS@,,;t t
s,@OMPI_BUILD_sds_env_DSO_TRUE@,,;t t
s,@OMPI_BUILD_sds_env_DSO_FALSE@,#,;t t
s,@OMPI_BUILD_sds_seed_DSO_TRUE@,,;t t
s,@OMPI_BUILD_sds_seed_DSO_FALSE@,#,;t t
s,@OMPI_BUILD_sds_singleton_DSO_TRUE@,,;t t
s,@OMPI_BUILD_sds_singleton_DSO_FALSE@,#,;t t
s,@OMPI_BUILD_sds_slurm_DSO_TRUE@,,;t t
s,@OMPI_BUILD_sds_slurm_DSO_FALSE@,#,;t t
s,@sds_bproc_CPPFLAGS@,,;t t
s,@sds_bproc_LDFLAGS@,,;t t
s,@sds_bproc_LIBS@,,;t t
s,@OMPI_BUILD_sds_bproc_DSO_TRUE@,,;t t
s,@OMPI_BUILD_sds_bproc_DSO_FALSE@,#,;t t
s,@OMPI_BUILD_sds_cnos_DSO_TRUE@,,;t t
s,@OMPI_BUILD_sds_cnos_DSO_FALSE@,#,;t t
s,@OMPI_BUILD_sds_pipe_DSO_TRUE@,,;t t
s,@OMPI_BUILD_sds_pipe_DSO_FALSE@,#,;t t
s,@sds_portals_utcp_CPPFLAGS@,,;t t
s,@sds_portals_utcp_LDFLAGS@,,;t t
s,@sds_portals_utcp_LIBS@,-lutcpapi -lutcplib -lp3api -lp3lib -lp3rt,;t t
s,@OMPI_BUILD_sds_portals_utcp_DSO_TRUE@,,;t t
s,@OMPI_BUILD_sds_portals_utcp_DSO_FALSE@,#,;t t
s,@MCA_sds_ALL_SUBDIRS@, env seed singleton slurm bproc cnos pipe portals_utcp,;t t
s,@MCA_sds_STATIC_SUBDIRS@,,;t t
s,@MCA_sds_DSO_SUBDIRS@, env seed singleton slurm pipe,;t t
s,@MCA_sds_STATIC_LTLIBS@,,;t t
s,@soh_bproc_CPPFLAGS@,,;t t
s,@soh_bproc_LDFLAGS@,,;t t
s,@soh_bproc_LIBS@,,;t t
s,@OMPI_BUILD_soh_bproc_DSO_TRUE@,,;t t
s,@OMPI_BUILD_soh_bproc_DSO_FALSE@,#,;t t
s,@MCA_soh_ALL_SUBDIRS@, bproc,;t t
s,@MCA_soh_STATIC_SUBDIRS@,,;t t
s,@MCA_soh_DSO_SUBDIRS@,,;t t
s,@MCA_soh_STATIC_LTLIBS@,,;t t
s,@MCA_orte_FRAMEWORKS@, errmgr gpr iof ns oob pls ras rds rmaps rmgr rml schema sds soh,;t t
s,@MCA_orte_FRAMEWORK_LIBS@, mca/errmgr/base/libmca_errmgr_base.la $(MCA_errmgr_STATIC_LTLIBS) mca/gpr/base/libmca_gpr_base.la $(MCA_gpr_STATIC_LTLIBS) mca/iof/base/libmca_iof_base.la $(MCA_iof_STATIC_LTLIBS) mca/ns/base/libmca_ns_base.la $(MCA_ns_STATIC_LTLIBS) mca/oob/base/libmca_oob_base.la $(MCA_oob_STATIC_LTLIBS) mca/pls/base/libmca_pls_base.la $(MCA_pls_STATIC_LTLIBS) mca/ras/base/libmca_ras_base.la $(MCA_ras_STATIC_LTLIBS) mca/rds/base/libmca_rds_base.la $(MCA_rds_STATIC_LTLIBS) mca/rmaps/base/libmca_rmaps_base.la $(MCA_rmaps_STATIC_LTLIBS) mca/rmgr/base/libmca_rmgr_base.la $(MCA_rmgr_STATIC_LTLIBS) mca/rml/base/libmca_rml_base.la $(MCA_rml_STATIC_LTLIBS) mca/schema/base/libmca_schema_base.la $(MCA_schema_STATIC_LTLIBS) mca/sds/base/libmca_sds_base.la $(MCA_sds_STATIC_LTLIBS) mca/soh/base/libmca_soh_base.la $(MCA_soh_STATIC_LTLIBS),;t t
s,@OMPI_BUILD_allocator_basic_DSO_TRUE@,,;t t
s,@OMPI_BUILD_allocator_basic_DSO_FALSE@,#,;t t
s,@OMPI_BUILD_allocator_bucket_DSO_TRUE@,,;t t
s,@OMPI_BUILD_allocator_bucket_DSO_FALSE@,#,;t t
s,@MCA_allocator_ALL_SUBDIRS@, basic bucket,;t t
s,@MCA_allocator_STATIC_SUBDIRS@,,;t t
s,@MCA_allocator_DSO_SUBDIRS@, basic bucket,;t t
s,@MCA_allocator_STATIC_LTLIBS@,,;t t
s,@OMPI_BUILD_bml_r2_DSO_TRUE@,,;t t
s,@OMPI_BUILD_bml_r2_DSO_FALSE@,#,;t t
s,@MCA_bml_ALL_SUBDIRS@, r2,;t t
s,@MCA_bml_STATIC_SUBDIRS@,,;t t
s,@MCA_bml_DSO_SUBDIRS@, r2,;t t
s,@MCA_bml_STATIC_LTLIBS@,,;t t
s,@OMPI_BUILD_btl_self_DSO_TRUE@,,;t t
s,@OMPI_BUILD_btl_self_DSO_FALSE@,#,;t t
s,@OMPI_BUILD_btl_sm_DSO_TRUE@,,;t t
s,@OMPI_BUILD_btl_sm_DSO_FALSE@,#,;t t
s,@btl_gm_CFLAGS@,,;t t
s,@btl_gm_CPPFLAGS@,,;t t
s,@btl_gm_LDFLAGS@,,;t t
s,@btl_gm_LIBS@,,;t t
s,@OMPI_BUILD_btl_gm_DSO_TRUE@,,;t t
s,@OMPI_BUILD_btl_gm_DSO_FALSE@,#,;t t
s,@btl_mvapi_CFLAGS@,-O3 -DNDEBUG -fno-strict-aliasing -pthread,;t t
s,@btl_mvapi_CPPFLAGS@,,;t t
s,@btl_mvapi_LDFLAGS@,,;t t
s,@btl_mvapi_LIBS@,,;t t
s,@OMPI_BUILD_btl_mvapi_DSO_TRUE@,,;t t
s,@OMPI_BUILD_btl_mvapi_DSO_FALSE@,#,;t t
s,@btl_mx_CFLAGS@,,;t t
s,@btl_mx_CPPFLAGS@,,;t t
s,@btl_mx_LDFLAGS@,,;t t
s,@btl_mx_LIBS@,,;t t
s,@OMPI_BUILD_btl_mx_DSO_TRUE@,,;t t
s,@OMPI_BUILD_btl_mx_DSO_FALSE@,#,;t t
s,@btl_openib_CFLAGS@,,;t t
s,@btl_openib_CPPFLAGS@,,;t t
s,@btl_openib_LDFLAGS@, ,;t t
s,@btl_openib_LIBS@,-lsysfs,;t t
s,@OMPI_BUILD_btl_openib_DSO_TRUE@,,;t t
s,@OMPI_BUILD_btl_openib_DSO_FALSE@,#,;t t
s,@btl_portals_CPPFLAGS@,,;t t
s,@btl_portals_LDFLAGS@,,;t t
s,@btl_portals_LIBS@,-lutcpapi -lutcplib -lp3api -lp3lib -lp3rt,;t t
s,@OMPI_BUILD_btl_portals_DSO_TRUE@,,;t t
s,@OMPI_BUILD_btl_portals_DSO_FALSE@,#,;t t
s,@OMPI_BUILD_btl_tcp_DSO_TRUE@,,;t t
s,@OMPI_BUILD_btl_tcp_DSO_FALSE@,#,;t t
s,@MCA_btl_ALL_SUBDIRS@, self sm gm mvapi mx openib portals llc tcp,;t t
s,@MCA_btl_STATIC_SUBDIRS@,,;t t
s,@MCA_btl_DSO_SUBDIRS@, self sm llc tcp,;t t
s,@MCA_btl_STATIC_LTLIBS@,,;t t
s,@OMPI_BUILD_coll_basic_DSO_TRUE@,,;t t
s,@OMPI_BUILD_coll_basic_DSO_FALSE@,#,;t t
s,@OMPI_BUILD_coll_self_DSO_TRUE@,,;t t
s,@OMPI_BUILD_coll_self_DSO_FALSE@,#,;t t
s,@OMPI_BUILD_coll_sm_DSO_TRUE@,,;t t
s,@OMPI_BUILD_coll_sm_DSO_FALSE@,#,;t t
s,@MCA_coll_ALL_SUBDIRS@, basic self sm,;t t
s,@MCA_coll_STATIC_SUBDIRS@,,;t t
s,@MCA_coll_DSO_SUBDIRS@, basic self sm,;t t
s,@MCA_coll_STATIC_LTLIBS@,,;t t
s,@OMPI_BUILD_common_sm_DSO_TRUE@,,;t t
s,@OMPI_BUILD_common_sm_DSO_FALSE@,#,;t t
s,@MCA_common_ALL_SUBDIRS@, sm,;t t
s,@MCA_common_STATIC_SUBDIRS@,,;t t
s,@MCA_common_DSO_SUBDIRS@, sm,;t t
s,@MCA_common_STATIC_LTLIBS@,,;t t
s,@OMPI_BUILD_io_romio_DSO_TRUE@,,;t t
s,@OMPI_BUILD_io_romio_DSO_FALSE@,#,;t t
s,@MCA_io_ALL_SUBDIRS@, romio,;t t
s,@MCA_io_STATIC_SUBDIRS@,,;t t
s,@MCA_io_DSO_SUBDIRS@, romio,;t t
s,@MCA_io_STATIC_LTLIBS@,,;t t
s,@OMPI_BUILD_mpool_sm_DSO_TRUE@,,;t t
s,@OMPI_BUILD_mpool_sm_DSO_FALSE@,#,;t t
s,@mpool_gm_CFLAGS@,,;t t
s,@mpool_gm_CPPFLAGS@,,;t t
s,@mpool_gm_LDFLAGS@,,;t t
s,@mpool_gm_LIBS@,,;t t
s,@OMPI_BUILD_mpool_gm_DSO_TRUE@,,;t t
s,@OMPI_BUILD_mpool_gm_DSO_FALSE@,#,;t t
s,@mpool_mvapi_CFLAGS@,-O3 -DNDEBUG -fno-strict-aliasing -pthread,;t t
s,@mpool_mvapi_CPPFLAGS@,,;t t
s,@mpool_mvapi_LDFLAGS@,,;t t
s,@mpool_mvapi_LIBS@,,;t t
s,@OMPI_BUILD_mpool_mvapi_DSO_TRUE@,,;t t
s,@OMPI_BUILD_mpool_mvapi_DSO_FALSE@,#,;t t
s,@mpool_openib_CFLAGS@,,;t t
s,@mpool_openib_CPPFLAGS@,,;t t
s,@mpool_openib_LDFLAGS@, ,;t t
s,@mpool_openib_LIBS@,-lsysfs,;t t
s,@OMPI_BUILD_mpool_openib_DSO_TRUE@,,;t t
s,@OMPI_BUILD_mpool_openib_DSO_FALSE@,#,;t t
s,@MCA_mpool_ALL_SUBDIRS@, sm gm mvapi openib,;t t
s,@MCA_mpool_STATIC_SUBDIRS@,,;t t
s,@MCA_mpool_DSO_SUBDIRS@, sm,;t t
s,@MCA_mpool_STATIC_LTLIBS@,,;t t
s,@OMPI_BUILD_pml_ob1_DSO_TRUE@,,;t t
s,@OMPI_BUILD_pml_ob1_DSO_FALSE@,#,;t t
s,@OMPI_BUILD_pml_teg_DSO_TRUE@,,;t t
s,@OMPI_BUILD_pml_teg_DSO_FALSE@,#,;t t
s,@MCA_pml_ALL_SUBDIRS@, ob1 teg,;t t
s,@MCA_pml_STATIC_SUBDIRS@,,;t t
s,@MCA_pml_DSO_SUBDIRS@, ob1 teg,;t t
s,@MCA_pml_STATIC_LTLIBS@,,;t t
s,@OMPI_BUILD_ptl_self_DSO_TRUE@,,;t t
s,@OMPI_BUILD_ptl_self_DSO_FALSE@,#,;t t
s,@OMPI_BUILD_ptl_sm_DSO_TRUE@,,;t t
s,@OMPI_BUILD_ptl_sm_DSO_FALSE@,#,;t t
s,@ptl_gm_CFLAGS@,,;t t
s,@ptl_gm_CPPFLAGS@,,;t t
s,@ptl_gm_LDFLAGS@,,;t t
s,@ptl_gm_LIBS@,,;t t
s,@OMPI_BUILD_ptl_gm_DSO_TRUE@,,;t t
s,@OMPI_BUILD_ptl_gm_DSO_FALSE@,#,;t t
s,@ptl_mx_CFLAGS@,,;t t
s,@ptl_mx_CPPFLAGS@,,;t t
s,@ptl_mx_LDFLAGS@,,;t t
s,@ptl_mx_LIBS@,,;t t
s,@OMPI_BUILD_ptl_mx_DSO_TRUE@,,;t t
s,@OMPI_BUILD_ptl_mx_DSO_FALSE@,#,;t t
s,@OMPI_BUILD_ptl_tcp_DSO_TRUE@,,;t t
s,@OMPI_BUILD_ptl_tcp_DSO_FALSE@,#,;t t
s,@MCA_ptl_ALL_SUBDIRS@, self sm gm mx tcp,;t t
s,@MCA_ptl_STATIC_SUBDIRS@,,;t t
s,@MCA_ptl_DSO_SUBDIRS@, self sm tcp,;t t
s,@MCA_ptl_STATIC_LTLIBS@,,;t t
s,@OMPI_BUILD_rcache_rb_DSO_TRUE@,,;t t
s,@OMPI_BUILD_rcache_rb_DSO_FALSE@,#,;t t
s,@MCA_rcache_ALL_SUBDIRS@, rb,;t t
s,@MCA_rcache_STATIC_SUBDIRS@,,;t t
s,@MCA_rcache_DSO_SUBDIRS@, rb,;t t
s,@MCA_rcache_STATIC_LTLIBS@,,;t t
s,@OMPI_BUILD_topo_unity_DSO_TRUE@,,;t t
s,@OMPI_BUILD_topo_unity_DSO_FALSE@,#,;t t
s,@MCA_topo_ALL_SUBDIRS@, unity,;t t
s,@MCA_topo_STATIC_SUBDIRS@,,;t t
s,@MCA_topo_DSO_SUBDIRS@, unity,;t t
s,@MCA_topo_STATIC_LTLIBS@,,;t t
s,@MCA_ompi_FRAMEWORKS@,common  allocator bml btl coll io mpool pml ptl rcache topo,;t t
s,@MCA_ompi_FRAMEWORK_LIBS@, mca/allocator/base/libmca_allocator_base.la $(MCA_allocator_STATIC_LTLIBS) mca/bml/base/libmca_bml_base.la $(MCA_bml_STATIC_LTLIBS) mca/btl/base/libmca_btl_base.la $(MCA_btl_STATIC_LTLIBS) mca/coll/base/libmca_coll_base.la $(MCA_coll_STATIC_LTLIBS) $(MCA_common_STATIC_LTLIBS) mca/io/base/libmca_io_base.la $(MCA_io_STATIC_LTLIBS) mca/mpool/base/libmca_mpool_base.la $(MCA_mpool_STATIC_LTLIBS) mca/pml/base/libmca_pml_base.la $(MCA_pml_STATIC_LTLIBS) mca/ptl/base/libmca_ptl_base.la $(MCA_ptl_STATIC_LTLIBS) mca/rcache/base/libmca_rcache_base.la $(MCA_rcache_STATIC_LTLIBS) mca/topo/base/libmca_topo_base.la $(MCA_topo_STATIC_LTLIBS),;t t
s,@MCA_pml_DIRECT_CALL_HEADER@,,;t t
s,@MCA_PROJECT_SUBDIRS@, opal orte ompi,;t t
s,@OMPI_LIBEVENT_SOURCES@,signal.c epoll_sub.c epoll.c poll.c select.c ,;t t
s,@LTDLINCL@,-I${top_srcdir}/opal/libltdl,;t t
s,@LIBLTDL@,${top_builddir}/opal/libltdl/libltdlc.la,;t t
s,@ECHO@,echo,;t t
s,@AR@,ar,;t t
s,@ac_ct_AR@,ar,;t t
s,@RANLIB@,ranlib,;t t
s,@ac_ct_RANLIB@,ranlib,;t t
s,@LIBTOOL@,$(SHELL) $(top_builddir)/libtool,;t t
s,@LIBLTDL_SUBDIR@,libltdl,;t t
s,@LIBLTDL_LTLIB@,libltdl/libltdlc.la,;t t
s,@WANT_LIBLTDL_TRUE@,,;t t
s,@WANT_LIBLTDL_FALSE@,#,;t t
s,@OPAL_LTDL_CPPFLAGS@,-I$(top_srcdir)/opal/libltdl,;t t
s,@CFLAGS_WITHOUT_OPTFLAGS@, -DNDEBUG -fno-strict-aliasing -pthread,;t t
s,@TOTALVIEW_DEBUG_FLAGS@,-g,;t t
s,@LIBMPI_EXTRA_LDFLAGS@,,;t t
s,@LIBMPI_EXTRA_LIBS@,,;t t
s,@CXXCPPFLAGS@,-I$(top_srcdir)/include -I$(top_srcdir) -I$(top_builddir) -I$(top_builddir)/include -I$(top_srcdir)/opal -I$(top_srcdir)/orte -I$(top_srcdir)/ompi  ,;t t
s,@WRAPPER_EXTRA_CFLAGS@,-pthread,;t t
s,@WRAPPER_EXTRA_CXXFLAGS@,-pthread,;t t
s,@WRAPPER_EXTRA_FFLAGS@,,;t t
s,@WRAPPER_EXTRA_FCFLAGS@,,;t t
s,@WRAPPER_EXTRA_LDFLAGS@,,;t t
s,@WRAPPER_EXTRA_LIBS@, -lutil -lnsl  -ldl  -Wl\,--export-dynamic -lm -lutil -lnsl -ldl,;t t
s,@LIBOBJS@,,;t t
s,@LTLIBOBJS@,,;t t
