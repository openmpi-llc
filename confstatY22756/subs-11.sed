:t
  /@[a-zA-Z_][a-zA-Z_0-9]*@/!b
s,@btl_openib_LIBS@,-lsysfs,;t t
s,@OMPI_BUILD_btl_openib_DSO_TRUE@,,;t t
s,@OMPI_BUILD_btl_openib_DSO_FALSE@,#,;t t
s,@btl_portals_CPPFLAGS@,,;t t
s,@btl_portals_LDFLAGS@,,;t t
s,@btl_portals_LIBS@,-lutcpapi -lutcplib -lp3api -lp3lib -lp3rt,;t t
s,@OMPI_BUILD_btl_portals_DSO_TRUE@,,;t t
s,@OMPI_BUILD_btl_portals_DSO_FALSE@,#,;t t
s,@OMPI_BUILD_btl_tcp_DSO_TRUE@,,;t t
s,@OMPI_BUILD_btl_tcp_DSO_FALSE@,#,;t t
s,@MCA_btl_ALL_SUBDIRS@, self sm gm mvapi mx openib portals llc tcp,;t t
s,@MCA_btl_STATIC_SUBDIRS@,,;t t
s,@MCA_btl_DSO_SUBDIRS@, self sm llc tcp,;t t
s,@MCA_btl_STATIC_LTLIBS@,,;t t
s,@OMPI_BUILD_coll_basic_DSO_TRUE@,,;t t
s,@OMPI_BUILD_coll_basic_DSO_FALSE@,#,;t t
s,@OMPI_BUILD_coll_self_DSO_TRUE@,,;t t
s,@OMPI_BUILD_coll_self_DSO_FALSE@,#,;t t
s,@OMPI_BUILD_coll_sm_DSO_TRUE@,,;t t
s,@OMPI_BUILD_coll_sm_DSO_FALSE@,#,;t t
s,@MCA_coll_ALL_SUBDIRS@, basic self sm,;t t
s,@MCA_coll_STATIC_SUBDIRS@,,;t t
s,@MCA_coll_DSO_SUBDIRS@, basic self sm,;t t
s,@MCA_coll_STATIC_LTLIBS@,,;t t
s,@OMPI_BUILD_common_sm_DSO_TRUE@,,;t t
s,@OMPI_BUILD_common_sm_DSO_FALSE@,#,;t t
s,@MCA_common_ALL_SUBDIRS@, sm,;t t
s,@MCA_common_STATIC_SUBDIRS@,,;t t
s,@MCA_common_DSO_SUBDIRS@, sm,;t t
s,@MCA_common_STATIC_LTLIBS@,,;t t
s,@OMPI_BUILD_io_romio_DSO_TRUE@,,;t t
s,@OMPI_BUILD_io_romio_DSO_FALSE@,#,;t t
s,@MCA_io_ALL_SUBDIRS@, romio,;t t
s,@MCA_io_STATIC_SUBDIRS@,,;t t
s,@MCA_io_DSO_SUBDIRS@, romio,;t t
s,@MCA_io_STATIC_LTLIBS@,,;t t
s,@OMPI_BUILD_mpool_sm_DSO_TRUE@,,;t t
s,@OMPI_BUILD_mpool_sm_DSO_FALSE@,#,;t t
s,@mpool_gm_CFLAGS@,,;t t
s,@mpool_gm_CPPFLAGS@,,;t t
s,@mpool_gm_LDFLAGS@,,;t t
s,@mpool_gm_LIBS@,,;t t
s,@OMPI_BUILD_mpool_gm_DSO_TRUE@,,;t t
s,@OMPI_BUILD_mpool_gm_DSO_FALSE@,#,;t t
s,@mpool_mvapi_CFLAGS@,-O3 -DNDEBUG -fno-strict-aliasing -pthread,;t t
s,@mpool_mvapi_CPPFLAGS@,,;t t
s,@mpool_mvapi_LDFLAGS@,,;t t
s,@mpool_mvapi_LIBS@,,;t t
