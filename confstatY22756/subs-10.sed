:t
  /@[a-zA-Z_][a-zA-Z_0-9]*@/!b
s,@soh_bproc_LIBS@,,;t t
s,@OMPI_BUILD_soh_bproc_DSO_TRUE@,,;t t
s,@OMPI_BUILD_soh_bproc_DSO_FALSE@,#,;t t
s,@MCA_soh_ALL_SUBDIRS@, bproc,;t t
s,@MCA_soh_STATIC_SUBDIRS@,,;t t
s,@MCA_soh_DSO_SUBDIRS@,,;t t
s,@MCA_soh_STATIC_LTLIBS@,,;t t
s,@MCA_orte_FRAMEWORKS@, errmgr gpr iof ns oob pls ras rds rmaps rmgr rml schema sds soh,;t t
s,@MCA_orte_FRAMEWORK_LIBS@, mca/errmgr/base/libmca_errmgr_base.la $(MCA_errmgr_STATIC_LTLIBS) mca/gpr/base/libmca_gpr_base.la $(MCA_gpr_STATIC_LTLIBS) mca/iof/base/libmca_iof_base.la $(MCA_iof_STATIC_LTLIBS) mca/ns/base/libmca_ns_base.la $(MCA_ns_STATIC_LTLIBS) mca/oob/base/libmca_oob_base.la $(MCA_oob_STATIC_LTLIBS) mca/pls/base/libmca_pls_base.la $(MCA_pls_STATIC_LTLIBS) mca/ras/base/libmca_ras_base.la $(MCA_ras_STATIC_LTLIBS) mca/rds/base/libmca_rds_base.la $(MCA_rds_STATIC_LTLIBS) mca/rmaps/base/libmca_rmaps_base.la $(MCA_rmaps_STATIC_LTLIBS) mca/rmgr/base/libmca_rmgr_base.la $(MCA_rmgr_STATIC_LTLIBS) mca/rml/base/libmca_rml_base.la $(MCA_rml_STATIC_LTLIBS) mca/schema/base/libmca_schema_base.la $(MCA_schema_STATIC_LTLIBS) mca/sds/base/libmca_sds_base.la $(MCA_sds_STATIC_LTLIBS) mca/soh/base/libmca_soh_base.la $(MCA_soh_STATIC_LTLIBS),;t t
s,@OMPI_BUILD_allocator_basic_DSO_TRUE@,,;t t
s,@OMPI_BUILD_allocator_basic_DSO_FALSE@,#,;t t
s,@OMPI_BUILD_allocator_bucket_DSO_TRUE@,,;t t
s,@OMPI_BUILD_allocator_bucket_DSO_FALSE@,#,;t t
s,@MCA_allocator_ALL_SUBDIRS@, basic bucket,;t t
s,@MCA_allocator_STATIC_SUBDIRS@,,;t t
s,@MCA_allocator_DSO_SUBDIRS@, basic bucket,;t t
s,@MCA_allocator_STATIC_LTLIBS@,,;t t
s,@OMPI_BUILD_bml_r2_DSO_TRUE@,,;t t
s,@OMPI_BUILD_bml_r2_DSO_FALSE@,#,;t t
s,@MCA_bml_ALL_SUBDIRS@, r2,;t t
s,@MCA_bml_STATIC_SUBDIRS@,,;t t
s,@MCA_bml_DSO_SUBDIRS@, r2,;t t
s,@MCA_bml_STATIC_LTLIBS@,,;t t
s,@OMPI_BUILD_btl_self_DSO_TRUE@,,;t t
s,@OMPI_BUILD_btl_self_DSO_FALSE@,#,;t t
s,@OMPI_BUILD_btl_sm_DSO_TRUE@,,;t t
s,@OMPI_BUILD_btl_sm_DSO_FALSE@,#,;t t
s,@btl_gm_CFLAGS@,,;t t
s,@btl_gm_CPPFLAGS@,,;t t
s,@btl_gm_LDFLAGS@,,;t t
s,@btl_gm_LIBS@,,;t t
s,@OMPI_BUILD_btl_gm_DSO_TRUE@,,;t t
s,@OMPI_BUILD_btl_gm_DSO_FALSE@,#,;t t
s,@btl_mvapi_CFLAGS@,-O3 -DNDEBUG -fno-strict-aliasing -pthread,;t t
s,@btl_mvapi_CPPFLAGS@,,;t t
s,@btl_mvapi_LDFLAGS@,,;t t
s,@btl_mvapi_LIBS@,,;t t
s,@OMPI_BUILD_btl_mvapi_DSO_TRUE@,,;t t
s,@OMPI_BUILD_btl_mvapi_DSO_FALSE@,#,;t t
s,@btl_mx_CFLAGS@,,;t t
s,@btl_mx_CPPFLAGS@,,;t t
s,@btl_mx_LDFLAGS@,,;t t
s,@btl_mx_LIBS@,,;t t
s,@OMPI_BUILD_btl_mx_DSO_TRUE@,,;t t
s,@OMPI_BUILD_btl_mx_DSO_FALSE@,#,;t t
s,@btl_openib_CFLAGS@,,;t t
s,@btl_openib_CPPFLAGS@,,;t t
s,@btl_openib_LDFLAGS@, ,;t t
