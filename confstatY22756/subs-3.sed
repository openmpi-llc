:t
  /@[a-zA-Z_][a-zA-Z_0-9]*@/!b
s,@WANT_MPI2_ONE_SIDED_TRUE@,#,;t t
s,@WANT_MPI2_ONE_SIDED_FALSE@,,;t t
s,@OMPI_F77_WIN_ATTR_KEYS@,,;t t
s,@OMPI_F77_WIN_ATTR_BASE_VALUE@,,;t t
s,@OMPI_F77_WIN_ATTR_SIZE_VALUE@,,;t t
s,@OMPI_F77_WIN_ATTR_DISP_VALUE@,,;t t
s,@OMPI_F77_WIN_NULL_COPY_FN@,,;t t
s,@OMPI_F77_WIN_NULL_DELETE_FN@,,;t t
s,@OMPI_F77_WIN_DUP_FN@,,;t t
s,@OMPI_FORTRAN_MAX_ARRAY_RANK@,4,;t t
s,@CC@,gcc,;t t
s,@CFLAGS@,-O3 -DNDEBUG -fno-strict-aliasing -pthread,;t t
s,@LDFLAGS@,-export-dynamic  ,;t t
s,@CPPFLAGS@,-I$(top_srcdir)/include -I$(top_srcdir) -I$(top_builddir) -I$(top_builddir)/include -I$(top_srcdir)/opal -I$(top_srcdir)/orte -I$(top_srcdir)/ompi  ,;t t
s,@ac_ct_CC@,gcc,;t t
s,@EXEEXT@,,;t t
s,@OBJEXT@,o,;t t
s,@DEPDIR@,.deps,;t t
s,@am__include@,include,;t t
s,@am__quote@,,;t t
s,@AMDEP_TRUE@,,;t t
s,@AMDEP_FALSE@,#,;t t
s,@AMDEPBACKSLASH@,\\,;t t
s,@CCDEPMODE@,depmode=gcc3,;t t
s,@am__fastdepCC_TRUE@,,;t t
s,@am__fastdepCC_FALSE@,#,;t t
s,@OMPI_CC_ABSOLUTE@,/usr/bin/gcc,;t t
s,@CPP@,gcc -E,;t t
s,@EGREP@,grep -E,;t t
s,@WANT_MPI_BINDINGS_LAYER_TRUE@,#,;t t
s,@WANT_MPI_BINDINGS_LAYER_FALSE@,,;t t
s,@WANT_PMPI_BINDINGS_LAYER_TRUE@,,;t t
s,@WANT_PMPI_BINDINGS_LAYER_FALSE@,#,;t t
s,@COMPILE_PROFILING_SEPARATELY_TRUE@,#,;t t
s,@COMPILE_PROFILING_SEPARATELY_FALSE@,,;t t
s,@CXX@,g++,;t t
s,@CXXFLAGS@,-O3 -DNDEBUG -finline-functions -pthread,;t t
s,@ac_ct_CXX@,g++,;t t
s,@CXXDEPMODE@,depmode=gcc3,;t t
s,@am__fastdepCXX_TRUE@,,;t t
s,@am__fastdepCXX_FALSE@,#,;t t
s,@CXXCPP@,g++ -E,;t t
s,@OMPI_CXX_ABSOLUTE@,/usr/bin/g++,;t t
s,@WANT_MPI_CXX_BINDINGS_TRUE@,,;t t
s,@WANT_MPI_CXX_BINDINGS_FALSE@,#,;t t
s,@CCAS@,gcc,;t t
s,@CCASFLAGS@,-O3 -DNDEBUG -fno-strict-aliasing,;t t
s,@OMPI_ASM_TEXT@,.text,;t t
