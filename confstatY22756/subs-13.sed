:t
  /@[a-zA-Z_][a-zA-Z_0-9]*@/!b
s,@OMPI_BUILD_topo_unity_DSO_TRUE@,,;t t
s,@OMPI_BUILD_topo_unity_DSO_FALSE@,#,;t t
s,@MCA_topo_ALL_SUBDIRS@, unity,;t t
s,@MCA_topo_STATIC_SUBDIRS@,,;t t
s,@MCA_topo_DSO_SUBDIRS@, unity,;t t
s,@MCA_topo_STATIC_LTLIBS@,,;t t
s,@MCA_ompi_FRAMEWORKS@,common  allocator bml btl coll io mpool pml ptl rcache topo,;t t
s,@MCA_ompi_FRAMEWORK_LIBS@, mca/allocator/base/libmca_allocator_base.la $(MCA_allocator_STATIC_LTLIBS) mca/bml/base/libmca_bml_base.la $(MCA_bml_STATIC_LTLIBS) mca/btl/base/libmca_btl_base.la $(MCA_btl_STATIC_LTLIBS) mca/coll/base/libmca_coll_base.la $(MCA_coll_STATIC_LTLIBS) $(MCA_common_STATIC_LTLIBS) mca/io/base/libmca_io_base.la $(MCA_io_STATIC_LTLIBS) mca/mpool/base/libmca_mpool_base.la $(MCA_mpool_STATIC_LTLIBS) mca/pml/base/libmca_pml_base.la $(MCA_pml_STATIC_LTLIBS) mca/ptl/base/libmca_ptl_base.la $(MCA_ptl_STATIC_LTLIBS) mca/rcache/base/libmca_rcache_base.la $(MCA_rcache_STATIC_LTLIBS) mca/topo/base/libmca_topo_base.la $(MCA_topo_STATIC_LTLIBS),;t t
s,@MCA_pml_DIRECT_CALL_HEADER@,,;t t
s,@MCA_PROJECT_SUBDIRS@, opal orte ompi,;t t
s,@OMPI_LIBEVENT_SOURCES@,signal.c epoll_sub.c epoll.c poll.c select.c ,;t t
s,@LTDLINCL@,-I${top_srcdir}/opal/libltdl,;t t
s,@LIBLTDL@,${top_builddir}/opal/libltdl/libltdlc.la,;t t
s,@ECHO@,echo,;t t
s,@AR@,ar,;t t
s,@ac_ct_AR@,ar,;t t
s,@RANLIB@,ranlib,;t t
s,@ac_ct_RANLIB@,ranlib,;t t
s,@LIBTOOL@,$(SHELL) $(top_builddir)/libtool,;t t
s,@LIBLTDL_SUBDIR@,libltdl,;t t
s,@LIBLTDL_LTLIB@,libltdl/libltdlc.la,;t t
s,@WANT_LIBLTDL_TRUE@,,;t t
s,@WANT_LIBLTDL_FALSE@,#,;t t
s,@OPAL_LTDL_CPPFLAGS@,-I$(top_srcdir)/opal/libltdl,;t t
s,@CFLAGS_WITHOUT_OPTFLAGS@, -DNDEBUG -fno-strict-aliasing -pthread,;t t
s,@TOTALVIEW_DEBUG_FLAGS@,-g,;t t
s,@LIBMPI_EXTRA_LDFLAGS@,,;t t
s,@LIBMPI_EXTRA_LIBS@,,;t t
s,@CXXCPPFLAGS@,-I$(top_srcdir)/include -I$(top_srcdir) -I$(top_builddir) -I$(top_builddir)/include -I$(top_srcdir)/opal -I$(top_srcdir)/orte -I$(top_srcdir)/ompi  ,;t t
s,@WRAPPER_EXTRA_CFLAGS@,-pthread,;t t
s,@WRAPPER_EXTRA_CXXFLAGS@,-pthread,;t t
s,@WRAPPER_EXTRA_FFLAGS@,,;t t
s,@WRAPPER_EXTRA_FCFLAGS@,,;t t
s,@WRAPPER_EXTRA_LDFLAGS@,,;t t
s,@WRAPPER_EXTRA_LIBS@, -lutil -lnsl  -ldl  -Wl\,--export-dynamic -lm -lutil -lnsl -ldl,;t t
s,@LIBOBJS@,,;t t
s,@LTLIBOBJS@,,;t t
