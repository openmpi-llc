/* -*- Mode: C; c-basic-offset:4 ; -*- */
/* 
 *   $Id: io_romio_ad_hfs_flush.c,v 1.6 2005/04/26 15:20:58 leslie Exp $    
 *
 *   Copyright (C) 1997 University of Chicago. 
 *   See COPYRIGHT notice in top-level directory.
 */

#include "ad_hfs.h"

void ADIOI_HFS_Flush(ADIO_File fd, int *error_code)
{
    ADIOI_GEN_Flush(fd, error_code);
}
