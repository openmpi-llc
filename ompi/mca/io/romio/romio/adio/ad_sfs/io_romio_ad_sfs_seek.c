/* -*- Mode: C; c-basic-offset:4 ; -*- */
/* 
 *   $Id: io_romio_ad_sfs_seek.c,v 1.6 2005/04/26 15:21:20 leslie Exp $    
 *
 *   Copyright (C) 1997 University of Chicago. 
 *   See COPYRIGHT notice in top-level directory.
 */

#include "ad_sfs.h"

ADIO_Offset ADIOI_SFS_SeekIndividual(ADIO_File fd, ADIO_Offset offset, 
		      int whence, int *error_code)
{
    return ADIOI_GEN_SeekIndividual(fd, offset, whence, error_code);
}
