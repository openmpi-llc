/* -*- Mode: C; c-basic-offset:4 ; -*- */
/* 
 *   $Id: io_romio_ad_nfs_flush.c,v 1.6 2005/04/26 15:21:01 leslie Exp $    
 *
 *   Copyright (C) 1997 University of Chicago. 
 *   See COPYRIGHT notice in top-level directory.
 */

#include "ad_nfs.h"

void ADIOI_NFS_Flush(ADIO_File fd, int *error_code)
{
    ADIOI_GEN_Flush(fd, error_code);
}
