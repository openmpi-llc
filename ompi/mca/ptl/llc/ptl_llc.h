/*
 * Copyright (c) 2004-2005 The Trustees of Indiana University and Indiana
 *                         University Research and Technology
 *                         Corporation.  All rights reserved.
 * Copyright (c) 2004-2005 The University of Tennessee and The University
 *                         of Tennessee Research Foundation.  All rights
 *                         reserved.
 * Copyright (c) 2004-2005 High Performance Computing Center Stuttgart, 
 *                         University of Stuttgart.  All rights reserved.
 * Copyright (c) 2004-2005 The Regents of the University of California.
 *                         All rights reserved.
 * $COPYRIGHT$
 * 
 * Additional copyrights may follow
 * 
 * $HEADER$
 */
/**
 * @file
 */
#ifndef MCA_PTL_LLC_H
#define MCA_PTL_LLC_H

#include "ompi_config.h"
#ifdef HAVE_SYS_TYPES_H
#include <sys/types.h>
#endif
#ifdef HAVE_SYS_SOCKET_H
#include <sys/socket.h>
#endif
#ifdef HAVE_NETINET_IN_H
#include <netinet/in.h>
#endif
#include "class/ompi_free_list.h"
#include "opal/event/event.h"
#include "mca/pml/pml.h"
#include "mca/ptl/ptl.h"

#define MCA_PTL_LLC_STATISTICS 0
#if defined(c_plusplus) || defined(__cplusplus)
extern "C" {
#endif

/**
 * LLC PTL module.
 */
struct mca_ptl_llc_component_t {
    mca_ptl_base_component_1_0_0_t super;  /**< base PTL component */
    struct mca_ptl_llc_module_t** llc_ptl_modules; /**< array of available PTL moduless */
    size_t llc_num_ptl_modules;            /**< number of ptls actually used */
    size_t llc_max_ptl_modules;            /**< maximum number of ptls - available kernel ifs */
    int llc_listen_sd;                     /**< listen socket for incoming connection requests */
    unsigned short llc_listen_port;        /**< listen port */
    char*  llc_if_include;                 /**< comma seperated list of interface to include */
    char*  llc_if_exclude;                 /**< comma seperated list of interface to exclude */
    int    llc_free_list_num;              /**< initial size of free lists */
    int    llc_free_list_max;              /**< maximum size of free lists */
    int    llc_free_list_inc;              /**< number of elements to alloc when growing free lists */
    int    llc_sndbuf;                     /**< socket sndbuf size */
    int    llc_rcvbuf;                     /**< socket rcvbuf size */
    size_t llc_frag_size;                  /**< buffer limit for the LLC PTL */
    ompi_free_list_t llc_send_frags;       /**< free list of llc send fragments */
    ompi_free_list_t llc_recv_frags;       /**< free list of llc recv fragments */
    opal_hash_table_t llc_procs;           /**< hash table of llc proc structures */
    opal_list_t llc_pending_acks;          /**< list of pending acks - retry as sends complete */
    opal_list_t llc_events;                /**< list of pending events */
    struct mca_ptl_llc_proc_t* llc_local;  /**< the llc proc instance corresponding to the local process */
    opal_event_t llc_send_event;           /**< event structure for sends */
    opal_event_t llc_recv_event;           /**< event structure for recvs */
    opal_mutex_t llc_lock;                 /**< lock for accessing module state */
};
typedef struct mca_ptl_llc_component_t mca_ptl_llc_component_t;
struct mca_ptl_llc_recv_frag_t;
struct mca_ptl_llc_send_frag_t;

OMPI_COMP_EXPORT extern mca_ptl_llc_component_t mca_ptl_llc_component;

/**
 * Register LLC module parameters with the MCA framework
 */
extern int mca_ptl_llc_component_open(void);

/**
 * Any final cleanup before being unloaded.
 */
extern int mca_ptl_llc_component_close(void);

/**
 * LLC module initialization.
 * 
 * @param num_ptls (OUT)                  Number of PTLs returned in PTL array.
 * @param allow_multi_user_threads (OUT)  Flag indicating wether PTL supports user threads (TRUE)
 * @param have_hidden_threads (OUT)       Flag indicating wether PTL uses threads (TRUE)
 *
 *  (1) read interface list from kernel and compare against module parameters
 *      then create a PTL instance for selected interfaces
 *  (2) setup LLC listen socket for incoming connection attempts
 *  (3) publish PTL addressing info 
 *
 */
extern mca_ptl_base_module_t** mca_ptl_llc_component_init(
    int *num_ptls, 
    bool enable_progress_threads,
    bool enable_mpi_threads
);

/**
 * LLC module control.
 */
extern int mca_ptl_llc_component_control(
    int param,
    void* value,
    size_t size
);

/**
 * LLC module progress.
 */
extern int mca_ptl_llc_component_progress(
   mca_ptl_tstamp_t tstamp
);

/**
 * LLC PTL Interface
 */
struct mca_ptl_llc_module_t {
    mca_ptl_base_module_t super;    /**< base PTL module interface */
    int                ptl_ifindex; /**< PTL interface index */
    struct sockaddr_in ptl_ifaddr;  /**< PTL interface address */
    struct sockaddr_in ptl_ifmask;  /**< PTL interface netmask */
    opal_list_t        ptl_peers;   /**< List of all peers for this PTL */
#if MCA_PTL_LLC_STATISTICS
    size_t ptl_bytes_sent;
    size_t ptl_bytes_recv;
    size_t ptl_send_handler;
#endif
};
typedef struct mca_ptl_llc_module_t mca_ptl_llc_module_t;

extern mca_ptl_llc_module_t mca_ptl_llc_module;


/**
 * Cleanup any resources held by the PTL.
 * 
 * @param ptl  PTL instance.
 * @return     OMPI_SUCCESS or error status on failure.
 */

extern int mca_ptl_llc_finalize(
    struct mca_ptl_base_module_t* ptl
);


/**
 * PML->PTL notification of change in the process list.
 * 
 * @param ptl (IN)
 * @param nprocs (IN)     Number of processes
 * @param procs (IN)      Set of processes
 * @param peers (OUT)     Set of (optional) peer addressing info.
 * @param peers (IN/OUT)  Set of processes that are reachable via this PTL.
 * @return     OMPI_SUCCESS or error status on failure.
 * 
 */

extern int mca_ptl_llc_add_procs(
    struct mca_ptl_base_module_t* ptl,
    size_t nprocs,
    struct ompi_proc_t **procs,
    struct mca_ptl_base_peer_t** peers,
    struct ompi_bitmap_t* reachable
);


/**
 * PML->PTL notification of change in the process list.
 *
 * @param ptl (IN)     PTL instance
 * @param nproc (IN)   Number of processes.
 * @param procs (IN)   Set of processes.
 * @param peers (IN)   Set of peer data structures.
 * @return             Status indicating if cleanup was successful
 *
 */
extern int mca_ptl_llc_del_procs(
    struct mca_ptl_base_module_t* ptl,
    size_t nprocs,
    struct ompi_proc_t **procs,
    struct mca_ptl_base_peer_t** peers
);

/**
 * PML->PTL Initialize a send request for LLC cache.
 *
 * @param ptl (IN)       PTL instance
 * @param request (IN)   Pointer to allocated request.
 *
 */
extern int mca_ptl_llc_request_init(
    struct mca_ptl_base_module_t* ptl,
    struct mca_ptl_base_send_request_t*
);

/**
 * PML->PTL Cleanup a send request that is being removed from the cache.
 *
 * @param ptl (IN)       PTL instance
 * @param request (IN)   Pointer to allocated request.
 *
 */
extern void mca_ptl_llc_request_fini(
    struct mca_ptl_base_module_t* ptl,
    struct mca_ptl_base_send_request_t*
);

/**
 * PML->PTL Notification that a receive fragment has been matched.
 *
 * @param ptl (IN)          PTL instance
 * @param recv_frag (IN)    Receive fragment
 *
 */
extern void mca_ptl_llc_matched(
    struct mca_ptl_base_module_t* ptl,
    struct mca_ptl_base_recv_frag_t* frag
);

/**
 * PML->PTL Initiate a send of the specified size.
 *
 * @param ptl (IN)               PTL instance
 * @param ptl_base_peer (IN)     PTL peer addressing
 * @param send_request (IN/OUT)  Send request (initialized by PML via mca_ptl_base_request_init_fn_t)
 * @param size (IN)              Number of bytes PML is requesting PTL to deliver
 * @param flags (IN)             Flags that should be passed to the peer via the message header.
 * @param request (OUT)          OMPI_SUCCESS if the PTL was able to queue one or more fragments
 */
extern int mca_ptl_llc_send(
    struct mca_ptl_base_module_t* ptl,
    struct mca_ptl_base_peer_t* ptl_peer,
    struct mca_ptl_base_send_request_t*,
    size_t offset,
    size_t size,
    int flags
);

/**
 * Return a recv fragment to the modules free list.
 *
 * @param ptl (IN)   PTL instance
 * @param frag (IN)  LLC receive fragment
 *
 */
extern void mca_ptl_llc_recv_frag_return(
    struct mca_ptl_base_module_t* ptl,
    struct mca_ptl_llc_recv_frag_t* frag
);



/**
 * Return a send fragment to the modules free list.
 *
 * @param ptl (IN)   PTL instance
 * @param frag (IN)  LLC send fragment
 *
 */
extern void mca_ptl_llc_send_frag_return(
    struct mca_ptl_base_module_t* ptl,
    struct mca_ptl_llc_send_frag_t*
);

#if defined(c_plusplus) || defined(__cplusplus)
}
#endif
#endif

