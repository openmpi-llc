/*
 * Copyright (c) 2004-2005 The Trustees of Indiana University and Indiana
 *                         University Research and Technology
 *                         Corporation.  All rights reserved.
 * Copyright (c) 2004-2005 The University of Tennessee and The University
 *                         of Tennessee Research Foundation.  All rights
 *                         reserved.
 * Copyright (c) 2004-2005 High Performance Computing Center Stuttgart, 
 *                         University of Stuttgart.  All rights reserved.
 * Copyright (c) 2004-2005 The Regents of the University of California.
 *                         All rights reserved.
 * $COPYRIGHT$
 * 
 * Additional copyrights may follow
 * 
 * $HEADER$
 */
/**
 * @file
 */
#ifndef MCA_BTL_LLC_ADDR_H
#define MCA_BTL_LLC_ADDR_H

#ifdef HAVE_SYS_TYPES_H
#include <sys/types.h>
#endif
#ifdef HAVE_SYS_SOCKET_H
#include <sys/socket.h>
#endif
#ifdef HAVE_NETINET_IN_H
#include <netinet/in.h>
#endif

/* LLC function specifics */
#include "llc.h"


/**
 * Structure used to publish LLC connection information to peers.
 */
struct mca_btl_llc_addr_t {
    struct sockaddr_llc addr_llc;     /**< LLC MAC address */
    in_port_t      addr_port;     /**< listen port */
    unsigned short addr_inuse;    /**< local meaning only */
};
typedef struct mca_btl_llc_addr_t mca_btl_llc_addr_t;

#endif

