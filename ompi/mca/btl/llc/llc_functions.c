#include<sys/socket.h>
#include<sys/types.h>
#include<netinet/in.h>
#include<unistd.h>
#include<stdlib.h>
#include<stdio.h>

#include <netdb.h>
#include <string.h>
#include <sys/time.h>

#define PROTOCOL_IP 0
#define CONNECT_PORT "38" //timeserver



#include <netinet/ether.h>
#include <linux/if.h>
#if 0
#include <linux/llc.h>
// till we have this header in glibc-devel
#else
#include "llc.h"
#endif

#ifndef AF_LLC
#define AF_LLC 26
#define PF_LLC 26
#endif

 
int
llcgetaddrinfo(const char *node, const char *service,
	       const struct addrinfo *hints,
	       struct addrinfo **res)
{
	char *x;
	unsigned char port;
	struct ether_addr e;
	struct addrinfo *ai;
	struct sockaddr_llc *sllc;
	int rc = EAI_NONAME;

	if (node && !ether_aton_r(node, &e) && ether_hostton(node, &e))
		goto out;
	rc = EAI_MEMORY;
	ai = malloc(sizeof(*ai));
	if (!ai)
		goto out_mem_ai;
	sllc = malloc(sizeof(*sllc));
	if (!sllc)
		goto out_mem_sllc;
	memset(ai, 0, sizeof(*ai));
	memset(sllc, 0, sizeof(*sllc));
	ai->ai_next	= 0;
	ai->ai_family	= AF_LLC;
	ai->ai_socktype = SOCK_STREAM;
	ai->ai_addrlen	= sizeof(*sllc);
	ai->ai_addr	= (struct sockaddr *)sllc;
	sllc->sllc_family = AF_LLC;
	sllc->sllc_arphrd = ARPHRD_ETHER;
	port		  = strtol(service ? service : "0", &x, 0);
	if (*x) { /* service is not numeric */
		struct servent* se = getservbyname(service, "llc");

		rc = EAI_SERVICE;
		if (!se)
			goto out_service;
		port = se->s_port;
	}
	if (node)
		memcpy(sllc->sllc_mac, e.ether_addr_octet, IFHWADDRLEN);
	sllc->sllc_sap = port;
	rc = 0;
	*res = ai;
out:
	return rc;
out_service:
	free(sllc);
out_mem_sllc:
	free(ai);
out_mem_ai:
	rc = EAI_MEMORY;
	goto out;
}

int
mygetaddrinfo(const char *node, const char *service,
	      const struct addrinfo *hints,
	      struct addrinfo **res)
{
	if (hints->ai_family == AF_LLC)
		return llcgetaddrinfo(node, service, hints, res);
	return getaddrinfo(node, service, hints, res);
}

int llcgetnameinfo(const struct sockaddr *sa, socklen_t salen, char *host,
		   size_t hostlen, char *serv, size_t servlen, int flags)
{
	struct sockaddr_llc *sllc = (struct sockaddr_llc *)sa;

	if (host && hostlen > 0) { /* resolve host */
		char name[64];
		struct ether_addr *eth = (struct ether_addr *)sllc->sllc_mac;
		if ((flags & NI_NUMERICHOST) ||
		    ether_ntohost(name, eth))
			ether_ntoa_r(eth, name);
		strncpy(host, name, hostlen - 1);
		host[hostlen - 1] = '\0';
	}
	if (serv && servlen > 0) { /* resolve service */
		struct servent *s;

		snprintf(serv, servlen - 1, "%d", sllc->sllc_sap);

		if (!(flags & NI_NUMERICHOST) &&
		    (s = getservbyport(sllc->sllc_sap, "llc") != NULL))
			strncpy(serv, s->s_name, servlen - 1);
		serv[servlen - 1] = '\0';
	}
	return 0;
}

int mygetnameinfo(const struct sockaddr *sa, socklen_t salen, char *host,
		  size_t hostlen, char *serv, size_t servlen, int flags)
{
	sa_family_t f = ((struct sockaddr_storage *)sa)->ss_family;

	if (f == AF_LLC)
		return llcgetnameinfo(sa, salen, host, hostlen,
				      serv, servlen, flags);
	return getnameinfo(sa, salen, host, hostlen, serv, servlen, flags);
}


static void get_hwaddr (char *name, struct ether_addr *addr, int *type)
{
  struct ifreq ifr;
  int fd = socket (AF_INET, SOCK_DGRAM, 0);

  if (fd < 0)
  {
    printf ("socket failed\n");
    return;
  }
  bcopy (name, &ifr.ifr_name, sizeof (ifr.ifr_name));

  /* find my own hardware address */
  if (ioctl (fd, SIOCGIFHWADDR, &ifr) < 0)
  {
    close (fd);
    printf ("ioctl(SIOCGIFHWADDR) failed\n");
    return;
  }
  close (fd);
  bcopy (&ifr.ifr_hwaddr.sa_data, addr, ETH_ALEN);
  *type = ifr.ifr_hwaddr.sa_family;
}
