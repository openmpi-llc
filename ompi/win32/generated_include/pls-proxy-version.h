/*
 * This file is automatically created by autogen.sh; it should not
 * be edited by hand!!
 *
 * List of version number for this component
 */

#ifndef MCA_pls_proxy_VERSION_H
#define MCA_pls_proxy_VERSION_H

#define MCA_pls_proxy_MAJOR_VERSION 1
#define MCA_pls_proxy_MINOR_VERSION 0
#define MCA_pls_proxy_RELEASE_VERSION 0
#define MCA_pls_proxy_ALPHA_VERSION 0
#define MCA_pls_proxy_BETA_VERSION 0
#define MCA_pls_proxy_SVN_VERSION "r6108"
#define MCA_pls_proxy_VERSION "1.0r6108"

#endif /* MCA_pls_proxy_VERSION_H */
