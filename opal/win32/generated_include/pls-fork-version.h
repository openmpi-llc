/*
 * This file is automatically created by autogen.sh; it should not
 * be edited by hand!!
 *
 * List of version number for this component
 */

#ifndef MCA_pls_fork_VERSION_H
#define MCA_pls_fork_VERSION_H

#define MCA_pls_fork_MAJOR_VERSION 1
#define MCA_pls_fork_MINOR_VERSION 0
#define MCA_pls_fork_RELEASE_VERSION 0
#define MCA_pls_fork_ALPHA_VERSION 0
#define MCA_pls_fork_BETA_VERSION 0
#define MCA_pls_fork_SVN_VERSION "r6108"
#define MCA_pls_fork_VERSION "1.0r6108"

#endif /* MCA_pls_fork_VERSION_H */
