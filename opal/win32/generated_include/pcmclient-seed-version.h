/* 
 * Copyright (c) 2004-2005 The Trustees of Indiana University and Indiana
 *                         University Research and Technology
 *                         Corporation.  All rights reserved.
 * Copyright (c) 2004-2005 The University of Tennessee and The University
 *                         of Tennessee Research Foundation.  All rights
 *                         reserved.
 * Copyright (c) 2004-2005 High Performance Computing Center Stuttgart, 
 *                         University of Stuttgart.  All rights reserved.
 * Copyright (c) 2004-2005 The Regents of the University of California.
 *                         All rights reserved.
 * $COPYRIGHT$
 * 
 * Additional copyrights may follow
 * 
 * $HEADER$
 */

/*
 * This file is automatically created by autogen.sh; it should not
 * be edited by hand!!
 *
 * List of version number for this component
 */

#ifndef MCA_pcmclient_seed_VERSION_H
#define MCA_pcmclient_seed_VERSION_H

#define MCA_pcmclient_seed_MAJOR_VERSION 1
#define MCA_pcmclient_seed_MINOR_VERSION 0
#define MCA_pcmclient_seed_RELEASE_VERSION 0
#define MCA_pcmclient_seed_ALPHA_VERSION 0
#define MCA_pcmclient_seed_BETA_VERSION 0
#define MCA_pcmclient_seed_SVN_VERSION "r3276"
#define MCA_pcmclient_seed_VERSION "1.0r3276"

#endif /* MCA_pcmclient_seed_VERSION_H */
