/*
 * This file is automatically created by autogen.sh; it should not
 * be edited by hand!!
 *
 * List of version number for this component
 */

#ifndef MCA_pls_rsh_VERSION_H
#define MCA_pls_rsh_VERSION_H

#define MCA_pls_rsh_MAJOR_VERSION 1
#define MCA_pls_rsh_MINOR_VERSION 0
#define MCA_pls_rsh_RELEASE_VERSION 0
#define MCA_pls_rsh_ALPHA_VERSION 0
#define MCA_pls_rsh_BETA_VERSION 0
#define MCA_pls_rsh_SVN_VERSION "r6108"
#define MCA_pls_rsh_VERSION "1.0r6108"

#endif /* MCA_pls_rsh_VERSION_H */
