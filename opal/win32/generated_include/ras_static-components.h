/*
 * $HEADER$
 */

extern const mca_base_component_t mca_ras_host_component;

const mca_base_component_t *mca_ras_base_static_components[] = {
   &mca_ras_host_component, 
  NULL
};
