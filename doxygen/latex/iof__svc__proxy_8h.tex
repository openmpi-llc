\hypertarget{iof__svc__proxy_8h}{
\section{orte/mca/iof/svc/iof\_\-svc\_\-proxy.h File Reference}
\label{iof__svc__proxy_8h}\index{orte/mca/iof/svc/iof_svc_proxy.h@{orte/mca/iof/svc/iof\_\-svc\_\-proxy.h}}
}
{\tt \#include \char`\"{}mca/iof/iof.h\char`\"{}}\par
{\tt \#include \char`\"{}ompi\_\-config.h\char`\"{}}\par
{\tt \#include \char`\"{}mca/iof/base/iof\_\-base\_\-header.h\char`\"{}}\par
\subsection*{Functions}
\begin{CompactItemize}
\item 
void \hyperlink{iof__svc__proxy_8h_a0}{orte\_\-iof\_\-svc\_\-proxy\_\-recv} (int status, orte\_\-process\_\-name\_\-t $\ast$peer, struct iovec $\ast$msg, int count, \hyperlink{rml__types_8h_a19}{orte\_\-rml\_\-tag\_\-t} tag, void $\ast$cbdata)
\begin{CompactList}\small\item\em Callback function from OOB on receipt of IOF request. \item\end{CompactList}\end{CompactItemize}


\subsection{Detailed Description}


\subsection{Function Documentation}
\hypertarget{iof__svc__proxy_8h_a0}{
\index{iof_svc_proxy.h@{iof\_\-svc\_\-proxy.h}!orte_iof_svc_proxy_recv@{orte\_\-iof\_\-svc\_\-proxy\_\-recv}}
\index{orte_iof_svc_proxy_recv@{orte\_\-iof\_\-svc\_\-proxy\_\-recv}!iof_svc_proxy.h@{iof\_\-svc\_\-proxy.h}}
\subsubsection[orte\_\-iof\_\-svc\_\-proxy\_\-recv]{\setlength{\rightskip}{0pt plus 5cm}void orte\_\-iof\_\-svc\_\-proxy\_\-recv (int {\em status}, orte\_\-process\_\-name\_\-t $\ast$ {\em peer}, struct iovec $\ast$ {\em iov}, int {\em count}, \hyperlink{rml__types_8h_a19}{orte\_\-rml\_\-tag\_\-t} {\em tag}, void $\ast$ {\em cbdata})}}
\label{iof__svc__proxy_8h_a0}


Callback function from OOB on receipt of IOF request. 

\begin{Desc}
\item[Parameters:]
\begin{description}
\item[{\em status}](IN) Completion status. \item[{\em peer}](IN) Opaque name of peer process. \item[{\em msg}](IN) Array of iovecs describing user buffers and lengths. \item[{\em count}](IN) Number of elements in iovec array. \item[{\em tag}](IN) User defined tag for matching send/recv. \item[{\em cbdata}](IN) User data. \end{description}
\end{Desc}
